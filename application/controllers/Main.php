<?php
require_once APPPATH.'/libraries/Conekta.php';
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('form_validation');
        date_default_timezone_set('America/Asuncion');
    }

    public function index() {
        $msj = "";
        $this->load->library('nikud');
        Conekta::setApiKey('key_vZWQqzn8Ykjr7tN9WCciuw');                
        if(isset($_POST) && count($_POST)>0){             
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('name','Nombre del cliente','required');
            $this->form_validation->set_rules('apellido','Apellido del cliente','required');
            $this->form_validation->set_rules('token','Aprobación','required');
            $this->form_validation->set_rules('total','Total','required|numeric');
            $this->form_validation->set_rules('ciclo','Ciclo','required');
            $this->form_validation->set_rules('duracion','Duracion','required|numeric');
            if($this->form_validation->run()){
                //Crear cliente en conekta
                try{
                    //Crear Plan
                    $plan = Conekta_Plan::where(array('name'=>"Plan para ".$_POST['email']));
                    if(!empty($plan) && count($plan)>0){
                        $plan = Conekta_Plan::find($plan[0]->id);
                        $plan->delete();
                    }
                    $plan = Conekta_Plan::create(array(                            
                            'name' => "Plan para ".$_POST['email'],
                            'amount' => $_POST['total'].'00',
                            'currency' => "MXN",
                            'interval' => $_POST['ciclo'],
                            'trial_period_days' => 0,
                            'expiry_count' => $_POST['duracion']
                    ));
                    $_POST['plan'] = $plan->id;
                    $clientes = Conekta_Customer::where(array('email'=>$_POST['email']));
                    $datos = array(
                        'name'=>$_POST['name'].' '.$_POST['apellido'],
                        'email'=>$_POST['email'],
                        'phone'=>$_POST['telefono'],
                        'cards'=>array(
                            $_POST['token']
                        ),
                        'plan'=>$_POST['plan']
                    );
                    
                    if(empty($clientes) || count($clientes)==0){
                        $cliente = Conekta_Customer::create($datos);
                    }else{
                        $cliente = Conekta_Customer::find($clientes[0]->id);
                        $datos['default_card_id'] = $cliente->cards[0]->id;                        
                        $cliente->update($datos);
                        $cliente = Conekta_Customer::find($clientes[0]->id);
                        if(empty($cliente->subscription)){
                            $cliente->createSubscription(array('plan'=>$_POST['plan']));                                                        
                        }
                        else{
                            $cliente->subscription->update(array('plan'=>$_POST['plan']));                        
                        }
                        
                    }
                }catch(Conekta_Error $e){
                    $error = $e->message_to_purchaser;
                }
                
                //Procesar Navixy
                if(empty($error)){
                    $error = $this->nikud->registrarCliente();
                }
            }else{                
                $error = $this->form_validation->error_string();
            }
            if(!empty($error)){                
                $emails = $this->nikud->getEmails();                
                $this->loadView(array('view'=>'main','msj'=>$error,'emails'=>$emails,'success'=>0));
            }else{
                header("Location:".base_url().'?success=1');
                exit;                
            }
        }else{
            $emails = $this->nikud->getEmails();
            $this->loadView(array('view'=>'main','emails'=>$emails,'success'=>0));
        }
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url());
        } else
            header("Location:" . base_url());
    }

    function pages($titulo) {
        $titulo = urldecode(str_replace("-", "+", $titulo));
        if (!empty($titulo)) {
            $pagina = $this->db->get_where('paginas', array('titulo' => $titulo));
            if ($pagina->num_rows() > 0) {
                $this->loadView(array('view' => 'paginas', 'contenido' => $pagina->row()->contenido, 'title' => $titulo));
            } else
                $this->loadView('404');
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $this->load->view('template', $param);
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }

    function sendMailer() {
        $this->load->library('mailer');
        $this->mailer->mail('joncar.c@gmail.com', 'Test', 'Test');
    }
    
    function procesarPagos(){               
        // Analizar la información del evento en forma de json
        $body = @file_get_contents('php://input');
        $event_json = json_decode($body);
        http_response_code(200); // Return 200 OK

        if ($event_json->type == 'charge.paid'){
            $detail = array(
                'email'=>$event_json->data->object->details->email,
                'monto'=>$event_json->data->object->amount,
                'description'=>$event_json->data->object->description
            );
            $this->load->library('nikud');
            $this->nikud->UserAddFonds($detail['email'],($detail['monto']/100),$detail['description']);
        }
    }

}
