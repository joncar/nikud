<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Seguridad extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function grupos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $crud->set_relation_n_n('funciones','funcion_grupo','funciones','grupo','funcion','{nombre}'); 
            $crud->set_relation_n_n('miembros','user_group','user','grupo','user','{nombre} {apellido}');                                 
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user($x = '',$y = ''){
            $this->norequireds = array('apellido_materno','foto');
            $crud = $this->crud_function($x,$y);  
            $crud->field_type('status','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('admin','true_false',array('0'=>'No','1'=>'Si'));
            $crud->field_type('estado_civil','enum',array('soltero'=>'Soltero','casado'=>'Casado','divorciado'=>'Divorciado','viudo'=>'Viudo'));
            $crud->field_type('sexo','enum',array('M'=>'Masculino','F'=>'Femenino'));
            $crud->field_type('password','password');
            $crud->field_type('repetir_password','password');            
            if($crud->getParameters()=='add'){
                $crud->set_rules('repetir_password','Repetir Password','required');            
                $crud->set_rules('cedula','Cedula','required|is_unique[user.cedula]');
            }
            $crud->unset_columns('password','');
            $crud->unset_fields('fecha_registro','fecha_actualizacion');
            $crud->set_field_upload('foto','img/fotos');
            if($crud->getParameters()=='edit' || $crud->getParameters()=='update' || $crud->getParameters()=='update_validation'){
                //$crud->field_type('cedula','hidden');
            }
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->columns('foto','cedula','nombre','apellido','email','status');
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function perfil($x = '',$y = ''){
            $this->as['perfil'] = 'user';
            $crud = $this->crud_function($x,$y);    
            $crud->where('id',$this->user->id);
            $crud->fields('nombre','apellido_paterno','apellido_materno','email','password','foto');
            $crud->field_type('password','password');
            $crud->field_type('foto','image',array('path'=>'img/fotos','width'=>'300px','height'=>'300px'));
            $crud->callback_before_update(function($post,$primary){
                if(!empty($primary) && $this->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'] || empty($primary)){
                    $post['password'] = md5($post['password']);                
                }
                return $post;
            });
            $crud->callback_after_update(function($post,$id){
                $this->user->login_short($id);
            });
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function user_insertion($post,$id = ''){
            if(!empty($id)){
                $post['pass'] = $this->db->get_where('user',array('id'=>$id))->row()->password!=$post['password']?md5($post['password']):$post['password'];
            }
            else $post['pass'] = md5($post['pass']);
            return $post;
        }
    }
?>
