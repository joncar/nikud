<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function planes($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->set_lang_string('insert_success_message','Sus datos han sido guardados con éxito <script>document.location.href="'.base_url('admin/planes_detalles').'/{id}/add"</script>');
            $output = $crud->render();
            $this->loadView($output);
        }               
        
        function planes_detalles($x = '',$y = ''){
            if(is_numeric($x)){
                $crud = $this->crud_function($x,$y);            
                $crud->where('planes_id',$x);                
                if($crud->getParameters()=='list'){
                    $crud->set_relation('planes_id','planes','planes_nombre');
                }else{
                    $crud->field_type('planes_id','hidden',$x);
                }
                $output = $crud->render();
                $this->loadView($output);
            }
        }
    }
?>
