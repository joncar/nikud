<?php

class Nikud
{
    protected $hash = "";
    protected $apiurl = "https://api.navixy.com/v2/panel/";
    protected $login = "11386";
    protected $password = "Jona1982";
    public function __construct() {
        
    }
    
    public function send($funct,$params){
              $data = $params;
              $ch = curl_init($this->apiurl.$funct);
              curl_setopt($ch, CURLOPT_POST, 1);
              curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
              curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
              $result = curl_exec($ch);
              curl_close($ch);
              $result = json_decode($result);
              return $result;
    }
    
    public function user_create($user = array()){
        if(empty($this->hash)){
            $this->panel_auth();
        }
        $response = $this->send('user/update',array(
            'hash'=>$this->hash,
            'user'=>json_encode($user),
            'locale'=>'es_ES',
            'time_zone'=>'America/Mexico_City'
            ));
        if($response->success==1){
            return $response;
        }else{            
            if($response->status->code==206){
                //Actualizar usuario);
                $account = $this->getUserFromEmail($user['login']);
                if($account){
                    $user['id'] = $account->id;
                    $response = $this->send('user/update',array(
                        'hash'=>$this->hash,
                        'user'=>json_encode($user),                
                    ));
                    return $response;
                }
            }
        }
    }
    
    public function panel_auth(){
        $response = $this->send('account/auth',array('login'=>$this->login,'password'=>$this->password));
        if($response->success==1){            
            $this->hash = $response->hash;
        }else{
            throw new Exception('Se ha producido un error, comunicandose con navixy',403);
            exit;
        }
    }
    
    public function getUserFromEmail($email){
        $list = $this->send('user/list',array('hash'=>$this->hash,'filter'=>$email));
        if(count($list->list)>0){
            return $list->list[0];
        }else{
            return null;
        }
    }
    
    public function UserAddFonds($email,$monto,$descripcion){
        if(empty($this->hash)){
            $this->panel_auth();
        }
        //List
        $user = $this->getUserFromEmail($email);
        if($user){
            //Read
            $data = array(
                "user_id"=>$user->id,
                "amount"=>$monto,
                "type"=>"balance",
                'hash'=>$this->hash,
                "text"=>"Recarga de saldo mediante Conekta por concepto de: ".$descripcion
            );
            $update = $this->send('user/transaction/change_balance',$data);
        }
    }
    
    public function registrarCliente(){   
        if(empty($this->hash)){
            $this->panel_auth();
        }
        $response = $this->send('user/list',array(
            'hash'=>$this->hash
        ));
        //Sacar id 
        foreach($response->list as $c){
            if($c->login==$_POST['email']){
                $id = $c->id;
            }
        }
        $response = $this->user_create(array(
            "id"=>$id,
            "login"=>$_POST['email'],
            "first_name"=>$_POST['name'],
            "middle_name"=>$_POST['apellido'],
            "last_name"=>$_POST['apellido'],
            "legal_type"=>"individual",
            "phone"=>(int)$_POST['telefono'],
            "post_country"=>"Mexican",        // country part of user's post address
            "post_index"=>$_POST['cp'],            // index part of user's post address
            "post_region"=>$_POST['region'],          // region part of user's post address
            "post_city"=>$_POST['ciudad'],         // city from postal address
            "post_street_address"=>$_POST['direccion'],
            "activated"=>true
        ));
        if(is_object($response) && $response->success==1){
            return '';
        }else{
            return 'Ha ocurrido un error con su registro, su caso ha sido notificado y en breve será resuelto. Disculpe por los inconvenientes';
        }
    }
    
    function getEmails(){
        if(empty($this->hash)){
            $this->panel_auth();
        }
        return $this->send('user/list',array('hash'=>$this->hash,'order_by'=>'login'));
    }
}

