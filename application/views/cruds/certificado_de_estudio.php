<?= $output ?>
<script>
    $(document).on('change','#field-user',function(){
            if($(this).val()!==''){
                $.post('<?= base_url('perceptoria/facturacion/getCedula') ?>',{cedula:$(this).val()},function(data){
                    data = JSON.parse(data);
                    if(data.error!==undefined){
                        $("#field-cedula").html('<span style="color:red">'+data.error+'</span>');
                        $("#field-cedula").show();                        
                        $("#field-estudiantes_carreras_id").hide();
                    }else{
                        $("#field-cedula").html(data.data);                        
                        $("#field-cedula").show();                                                
                        $("#field-estudiantes_carreras_id").replaceWith(data.estudiantes_carreras);
                        $("#field-estudiantes_carreras_id").show();                        
                    }
                });
            }
        });
</script>