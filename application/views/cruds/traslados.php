<?= $output ?>
<script>
    $("#field-cedulaLabel").change(function(){
        var enc = false;
        console.log(estudiantes);
        for(var i in estudiantes)
        {
            if(estudiantes[i].cedula===$(this).val()){
                enc = true;
                $(this).parents('.form-group').removeClass('has-error').addClass('has-success');
                $(this).val(estudiantes[i].cedula+' '+estudiantes[i].nombre+' '+estudiantes[i].apellido_paterno);
                $("#field-programacion_carrera_id_origen").val(estudiantes[i].programacion_carreras_id);
                $("#field-estudiantes_id").val(estudiantes[i].id);
                $("#field-programacion_carrera_id_origen").chosen().trigger('liszt:updated');
            }
        }
        if(!enc){ 
                $(this).parents('.form-group').removeClass('has-success').addClass('has-error');
                $(this).val('Estudiante no encontrado');            
        }
    });
</script>