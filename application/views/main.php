<div class="page-header">

        <h1>

                 <i class="ace-icon fa fa-credit-card"></i> Afiliación de servicio

        </h1>

</div><!-- /.page-header -->

<?php if(empty($_GET['success'])): ?>
<div class="row">
    <div class="col-xs-12 col-sm-10 col-sm-offset-1 well">
        <form id="registrar" action="" method="post" onsubmit="return tokenizar()">
            
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="email">Correo de usuario *</label>
                      <select class="form-control chosen-select" name="email" id="email" required>                
                            <option value="">Seleccione un email</option>
                        <?php foreach($emails->list as $e): ?>
                            <option value="<?= $e->login ?>"><?= $e->login ?></option>
                        <?php endforeach ?>
                      </select>              
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="email">Nombre del cliente *</label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Nombre y Apellido" required>
                    </div>
            </div>
            
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="email">Apellido del cliente *</label>
                      <input type="text" name="apellido" class="form-control" id="apellido" placeholder="Nombre y Apellido" required>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="email">Telefono *</label>
                      <input type="text" name="telefono" class="form-control" id="telefono" placeholder="Telefono" required>
                    </div>
            </div>
            
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="email">Estado *</label>
                      <input type="text" name="region" class="form-control" id="region" placeholder="Estado de habitación" required>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="email">Ciudad *</label>
                      <input type="text" name="ciudad" class="form-control" id="ciudad" placeholder="Ciudad" required>
                    </div>
            </div>
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="email">Dirección *</label>
                      <input type="text" name="direccion" class="form-control" id="direccion" placeholder="Dirección" required>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="email">Código Postal *</label>
                      <input type="text" name="cp" class="form-control" id="cp" placeholder="Código CP" required>
                    </div>
            </div>
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="nombre">Nombre en la tarjeta *</label>
                      <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre en la tarjeta" required>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="tarjeta">Numero de tarjeta *</label>
                      <input type="number" name="tarjeta" class="form-control" id="tarjeta" placeholder="Numero de tarjeta" required>
                    </div>
            </div>
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="cvc">CVC *</label>
                      <input type="number" name="cvc" size="3" class="form-control" id="cvc" placeholder="Código de Seguridad" required>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="mes">Mes de vencimiento *</label>
                      <select class="form-control" name="mes" required>
                        <?php for($i=1;$i<13;$i++): ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor ?>
                      </select>
                    </div>
            </div>
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="anio">Año de vencimiento *</label>
                      <select class="form-control" name="anio" required>
                        <?php for($i=date("Y");$i<date("Y")+20;$i++): ?>
                        <option value="<?= $i ?>"><?= $i ?></option>
                        <?php endfor ?>
                      </select>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="anio">Cantidad Equipos</label>
                      <input type="number" name="equipos" class="form-control" id="equipos" placeholder="Cantidad de equipos" required>
                    </div>
            </div>
            <div class="row">
                    <div class="form-group col-xs-6">
                      <label for="anio">Precio x Equipo</label>
                      <input type="number" name="precio" class="form-control" id="precio" placeholder="Precio de equipos" required>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="anio">Total</label>
                      <input type="number" name="total" class="form-control" id="total" placeholder="Total" required>
                    </div>
            </div>
            <div class="row">
                    <div class="form-group col-xs-6">
                        <label for="anio">Frecuencia</label>
                        <select name="ciclo" id="ciclo" class="form-control">
                            <option value="week" data-val="48">Semanal</option>
                            <!--<option value="half_month">Quincenal</option>-->
                            <option value="month" data-val="12" selected="">Mensual</option>
                            <option value="year" data-val="1">Anual</option>
                        </select>
                    </div>
                    <div class="form-group col-xs-6">
                      <label for="anio">Duración en meses</label>
                      <input type="number" name="duracion" class="form-control" id="duracion" placeholder="Duración en meses" value="12" required>
                    </div>
            </div>
            <div class="row">
                    <input type="hidden" name="token" value="">
                    <button id="boton" type="submit" class="btn btn-default btn-block">Cargar Cliente</button>
                    <div style="text-align: center" class="hidden load">
                        <i class="fa fa-spinner fa-5x loading" style="color:#438eb9"></i>
                        <div>Procesando Registro</div>
                    </div>
                    <div class="alert alert-danger <?= !empty($msj)?'':'hidden' ?>"><?= !empty($msj)?$msj:'' ?></div>
            </div>
          </form>
    </div>
</div><!-- /.row -->
<?php $this->load->view('predesign/chosen'); ?>
<script type="text/javascript" src="//conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>  
<script>
    var clientes = <?= json_encode($emails->list) ?>;
    
    $(document).on('ready',function(){
        $("#email").change(function(){
            if($(this).val()!==''){
                for(var i in clientes){
                    if(clientes[i].login===$(this).val()){
                        $("#name").val(clientes[i].first_name);
                        $("#apellido").val(clientes[i].last_name);
                        $("#telefono").val(clientes[i].phone);
                        $("#ciudad").val(clientes[i].post_city);
                        $("#region").val(clientes[i].post_region);
                        $("#direccion").val(clientes[i].post_street_address);
                        $("#cp").val(clientes[i].post_index);
                    }
                }
            }
        });
        
        $("#equipos,#precio").change(function(){
           var equipos = parseInt($("#equipos").val());
           var precio = parseFloat($("#precio").val());
           var total = equipos*precio;
           $("#total").val(total);
        });
        
        $("#ciclo").change(function(){
            $("#duracion").val($("#ciclo option:selected").data('val'));
        });
    });
    
    function tokenizar(){        
        if($("input[name='token']").val()===''){
            $(".load").removeClass('hidden');
            $("#boton").attr('disabled',true);
            var tarjeta = '';
            Conekta.setPublishableKey('key_YoxQT3gaxwePKRbiFCEtkGg');
            $form = {
                card:{
                    'name':$("input[name='nombre']").val(),
                    'number':$("input[name='tarjeta']").val(),
                    'cvc':$("input[name='cvc']").val(),
                    'exp_month':$("select[name='mes']").val(),
                    'exp_year':$("select[name='anio']").val()
                }
            };

            Conekta.token.create($form,
            function(data){            
                $("input[name='token']").val(data.id);
                $("#registrar").submit();
            }, 
            function(data){            
                $(".alert-danger").html('Disculpe pero su transacción no fué aprobada ['+data.message_to_purchaser+']').removeClass('hidden');
                $(".load").addClass('hidden');
                $("#boton").attr('disabled',false);
                console.log(data);
            });
            return false;
        }else{
            return true;
        }
    }
</script>
<?php else: ?>
<div class="alert alert-success">
    Gracias por afiliarse a nuestro servicio.
</div>
<?php endif; ?>
